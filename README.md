# Plotting voxel-level tumor control probability

Plotting the voxel-level empirical Tumor Control Probability (TCP) or linear-quadratic Poisson model with the following formulas:

![Tumor control probability](definition-of-tumor-control-probability.png)

## Requirements
I recommend installing using a virtual environment (optional):
```
python3 -m venv venv              # Create virtual environment.
source venv/bin/activate          # Activate virt. env.
pip install --upgrade pip         # Upgrade pip.
pip install -r requirements.txt   # Install requirements.
```

Install requirements:

```
pip install -r requirements.txt
```

## Execute

```
python3 plot.py
```

## Example plots

![Example plot](example_plot.png)
![Example plot 2](example_plot2.png)
![Example plot 3](example_plot3.png)