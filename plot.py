from pyradiobiology import * 
import matplotlib.pyplot as plt
import numpy as np
from tkinter import *
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg


class PlotHandler:
  def __init__(self, root):
    self.root = root
    self.root.title("Voxel level response plotter.")
    self.root.geometry("960x540")

    self.d50 = 10
    self.gamma = 2.63
    self.doses = DoseBag(data=[*np.arange(start=0, stop=25, step=0.1)], 
        unit=DoseUnit.GY, 
        dose_type=DoseType.EQD2)
    self.model = TcpEmpirical(d50=Dose.gy(self.d50, DoseType.EQD2),
        gamma=self.gamma)

    # D50
    Label(self.root, text="D50").grid(row=0, column=0)
    val = IntVar()
    self.d50_entry = Entry(self.root, width=5, text=val)
    val.set(str(self.d50))
    self.d50_entry.grid(row=0, column=1)

    # Gamma
    Label(self.root, text="gamma").grid(row=1, column=0)
    val = IntVar()
    self.gamma_entry = Entry(self.root, width=5, text=val)
    val.set(str(self.gamma))
    self.gamma_entry.grid(row=1, column=1)

    # Update Button
    button = Button(self.root, text="Calculate", command=self.update_values)
    button.grid(row=2, column=0)
    self.root.bind("<Return>", self.update_values)
    self.plot_values()    

  def update_values(self):
    self.d50 = float(self.d50_entry.get())
    self.gamma = float(self.gamma_entry.get())

    self.model = TcpEmpirical(d50=Dose.gy(self.d50, DoseType.EQD2),
        gamma=self.gamma)
    self.plot_values()

  def plot_values(self):
    tcp_voxels = self.model.voxel_response(self.doses)    

    figure = plt.figure(figsize=(8, 4.5))
    plt.plot(self.doses.data, tcp_voxels, linewidth=4)
    plt.xlabel("EQD2 (Gy)")
    plt.ylabel("TCP")
    plt.grid()

    chart = FigureCanvasTkAgg(figure, self.root)
    chart.get_tk_widget().grid(row=3, column=2)

def main():
  root = Tk()
  gui = PlotHandler(root)
  gui.root.mainloop()

if __name__ == "__main__":
  main()